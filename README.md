# Framework Charge Limiter

## Credits

I personally take no credit for this. All credit should go to [Tim Ruffing](https://gist.github.com/real-or-random). I simply hope to bundle this into something that is installable.

## Usage

This looks to correct a current issue in Framework Intel 12th generation laptops around charging / discharging when a battery charge limit is set. More details can be found in the thread below.

[https://community.frame.work/t/tracking-battery-flipping-between-charging-and-discharging-draws-from-battery-even-on-ac/22484/65?u=christopher_bates](https://community.frame.work/t/tracking-battery-flipping-between-charging-and-discharging-draws-from-battery-even-on-ac/22484/65)



## Requirements

This package requires on the framewwork-ec package be installed, and this is a modification of the Google EC tool built for Framework devices.

https://github.com/DHowett/framework-ec