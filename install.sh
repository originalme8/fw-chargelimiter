#!/usr/bin/bash

# Copy charge-limiter.sh and restart-charge-limititer.sh to /usr/local/sbin
# Adapted from https://gist.github.com/ahmedsadman/2c1f118a02190c868b33c9c71835d706

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

SERVICE_DIR="/etc/systemd/system"
SERVICE_NAME="fw-charge-limiter"
BIN_DIR="/usr/local/sbin"
INST_DIR="/opt/fw-charge-control"

if [ "$1" = "remove" ]; then


    sudo systemctl stop ${SERVICE_NAME}.service
    sudo systemctl disable ${SERVICE_NAME}.service
    rm "${BIN_DIR}/fw-charge-limiter.sh"
    rm "${BIN_DIR}/fw-restart-charge-limiter.sh"
    rm -rf "$INST_DIR"
    rmdir "$INST_DIR"
    rm  "${SERVICE_DIR}/fw-charge-control.service"
    journalctl "$SERVICE_NAME has been removed successfully from the system"

elif [ -z $1 ]; then
    which ectool > /dev/null
    RETURN="$?"

    # Validate that ectool is installed
    if [ "${RETURN}" != "0" ] ; then
        echo "ectool not detected, this is arequirement. Please install it and re-run this installer."
        echo "Exiting"
        exit 1

    fi

    # Copy contents to install directory (Default: /opt/fw-charge-control)
    
    mkdir -p "${INST_DIR}"
    cp ./fw-charge-limiter.sh $INST_DIR/fw-charge-limiter.sh 
    cp ./fw-restart-charge-limiter.sh $INST_DIR/fw-restart-charge-limiter.sh

    chown -R "$INST_DIR/"
    chmod 550 "$INST_DIR/fw-charge-limiter.sh"
    chmod 550 "$INST_DIR/fw-restart-charge-limiter.sh"


    # Copy files to correct location

    ln -s ${INST_DIR}/*.sh /${BIN_DIR}/
    cp ./fw-charge-limiter.sh $BIN_DIR
    cp ./fw-restart-charge-limiter.sh $BIN_DIR

    # Setup service
    cp ./fw-charge-control.service "${SERVICE_DIR}/fw-charge-control.service"
    

    # Start and enable service

    systemctl daemon-reload
    systemctl enable --now fw-charge-control.service

fi
exit 0