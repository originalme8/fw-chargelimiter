#!/bin/sh

# Store as /usr/lib/systemd/system-sleep/restart-charge-limiter.sh and make executable

case $1 in
    pre)  ;;
    post) systemctl try-restart charge-limiter ;;
esac