#!/bin/bash

# Store as /etc/systemd/system/charge-limiter.sh and make executable

# BIOS/fwchargelimit is 65
min=60

NO_LIMIT=4294967295 # -1u
LOW=100

setting=-1
set_limit() {
    target=$1
    if [ "$setting" -ne "$target" ]; then
        if [ "$target" -ne "$LOW" ]; then
            echo "Setting chargecurrentlimit to $LOW before setting it to $target"
            ectool chargecurrentlimit $LOW;
            # Let the value settle down a bit. The goal is to avoid fast switching.
            sleep 20;
        fi
        echo "Setting chargecurrentlimit to $target"
        ectool chargecurrentlimit $target;
        setting=$target
        force=false
    fi
}

stop() {
    # Do nothing when the system is shutting down, to avoid sleep delays.
    if [ "$(systemctl is-system-running || true)" != "stopping" ]; then
        set_limit $NO_LIMIT
    else
        echo "Skipping reset because system is shutting down."
    fi
    exit
}

# Reset limit on exit
trap 'stop' EXIT
# Break the wait on USR1
trap '' USR1
# Force rewriting on HUP (well, restarting the script is easier...)
trap 'setting=-1' HUP

while true; do
    read cur < /sys/class/power_supply/BAT1/capacity
    if [ "$cur" -ge "$min" ]; then
        set_limit $LOW
    else
        set_limit $NO_LIMIT
    fi
    sleep 30 &
    wait $!
done